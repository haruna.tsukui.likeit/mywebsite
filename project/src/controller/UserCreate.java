package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDAO;

/**
 * Servlet implementation class UserCreate
 */
@WebServlet("/UserCreate")
public class UserCreate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserCreate() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String passwordConf = request.getParameter("passwordConf");
		String userName = request.getParameter("userName");
		String birthDate = request.getParameter("birthDate");
		String address = request.getParameter("address");

		UserDAO userDAO = new UserDAO();

		boolean bo = userDAO.idCreate(loginId);

		if (!(password.equals(passwordConf))) {
			request.setAttribute("inputLoginId",loginId);
			request.setAttribute("inputuserName",userName);
			request.setAttribute("inputbirthDate",birthDate);
			request.setAttribute("inputaddress",address);
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
			dispatcher.forward(request, response);
		} else if (loginId.equals("") || password.equals("") || passwordConf.equals("") || userName.equals("")
				|| birthDate.equals("") || address.equals("")) {
			request.setAttribute("inputLoginId",loginId);
			request.setAttribute("inputuserName",userName);
			request.setAttribute("inputbirthDate",birthDate);
			request.setAttribute("inputaddress",address);
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
			dispatcher.forward(request, response);
		} else if (!bo) {
			request.setAttribute("inputuserName",userName);
			request.setAttribute("inputbirthDate",birthDate);
			request.setAttribute("inputaddress",address);
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
			dispatcher.forward(request, response);
		} else {
			userDAO.userCreate(userName, address,loginId, password, birthDate);
			response.sendRedirect("Login");
		}

	}

}
