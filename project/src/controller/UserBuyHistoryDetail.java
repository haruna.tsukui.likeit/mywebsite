package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.ItemDataBeans;
import dao.BuyDAO;
import dao.BuyDetailDAO;

/**
 * Servlet implementation class UserBuyHistoryDetail
 */
@WebServlet("/UserBuyHistoryDetail")
public class UserBuyHistoryDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		try {

			request.setCharacterEncoding("UTF-8");
			int id = Integer.parseInt(request.getParameter("id"));

			//コンソール確認用
			System.out.println(id);

			BuyDataBeans bdb = BuyDAO.getBuyDataBeansByBuyId(id);

			request.setAttribute("bdb", bdb);

			ArrayList<ItemDataBeans> buyIDBList = BuyDetailDAO.getItemDataBeansListByBuyId(id);

			request.setAttribute("buyIDBList", buyIDBList);

			BuyDataBeans resultBDB = BuyDAO.getBuyDataBeansByBuyId(id);

			request.setAttribute("resultBDB", resultBDB);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userBuyHistoryDetail.jsp");
			dispatcher.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
