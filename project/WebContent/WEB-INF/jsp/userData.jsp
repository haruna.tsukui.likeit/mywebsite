<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー情報</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/original/common.css">

</head>
<body>

	<!-- header -->
	<header>
		<nav class="navbar navbar-expand-lg navbar-light"
			style="background-color: #17a2b8;">
			<a class="navbar-brand" href="Index">EC</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarNav" aria-controls="navbarNav"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<li class="nav-item"><a class="nav-link" href="UserData">User</a></li>
					<li class="nav-item"><a class="nav-link" href="Cart">Cart</a></li>
					<li class="nav-item"><a class="nav-link" href="Login">Logout</a></li>
				</ul>
			</div>
		</nav>
	</header>


	<div class="container">
		<div class="row center">
			<h5>ユーザー情報</h5>
		</div>
		<a class="btn btn-outline-info" href="UserUpdate?id=${userInfo.id}">更新</a>

		<div align="right">
			<a href="UserCreate" class="text-info">新規登録</a>
		</div>


	</div>
	<br>




	<div class="container">

		<div class="row">
			<div class="col s12">
				<table class="table">
					<thead>
						<tr>
							<th style="width: 10%"></th>
							<th class="center">購入日時</th>
							<th class="center">配送方法</th>
							<th class="center">購入金額</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="resultBDB" items="${resultBDB}">
							<tr>
								<td class="center"><a
									href="UserBuyHistoryDetail?id=${resultBDB.id}"
									class="btn btn-outline-info"> ⇩</a></td>
								<td class="center">${resultBDB.formatDate}</td>
								<td class="center">${resultBDB.deliveryMethodName}</td>
								<td class="center">${resultBDB.totalPrice}円</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>


</body>
</html>