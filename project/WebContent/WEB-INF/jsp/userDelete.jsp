<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー削除</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/original/common.css">

</head>
<body>


	<!-- header -->
	<header>
		<nav class="navbar navbar-expand-lg navbar-light"
			style="background-color: #17a2b8;">
			<a class="navbar-brand" href="Index">EC</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarNav" aria-controls="navbarNav"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<li class="nav-item"><a class="nav-link" href="UserData">User</a></li>
					<li class="nav-item"><a class="nav-link" href="Cart">Cart</a></li>
					<li class="nav-item"><a class="nav-link" href="Login">Logout</a></li>
				</ul>
			</div>
		</nav>
	</header>
	<!-- /header -->

	<!-- header -->
	<header>
		<nav class="navbar navbar-expand-lg navbar-light"
			style="background-color: #17a2b8;">
			<a class="navbar-brand" href="Index">EC</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarNav" aria-controls="navbarNav"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<li class="nav-item"><a class="nav-link" href="UserData">User</a></li>
					<li class="nav-item"><a class="nav-link" href="Cart">Cart</a></li>
					<li class="nav-item"><a class="nav-link" href="Login">Logout</a></li>
				</ul>
			</div>
		</nav>
	</header>
	<!-- /header -->


	<div class="userDelete text-center">
		<p>${userInfo.name}</p>
		<p>を本当に削除してよろしいでしょうか。</p>
		<form method="post" action="UserDelete">
			<input type="hidden" name="id" value="${user.id}">
			<div class="row justify-content-center">
				<div class="col-4">
					<a href="UserList" class="btn btn-outline-info text-info">キャンセル</a>
				</div>
				<div class="col-4">
					<p>
						<input type="submit" value="OK"
							class="btn btn-outline-info text-info">
					</p>
				</div>
			</div>
		</form>
	</div>
</body>
</html>