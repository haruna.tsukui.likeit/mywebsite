<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>userList</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/original/common.css">

</head>
<body>

	<!-- header -->
	<header>
		<nav class="navbar navbar-expand-lg navbar-light"
			style="background-color: #17a2b8;">
			<a class="navbar-brand" href="Index">EC</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarNav" aria-controls="navbarNav"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<li class="nav-item"><a class="nav-link" href="UserData">User</a></li>
					<li class="nav-item"><a class="nav-link" href="Cart">Cart</a></li>
					<li class="nav-item"><a class="nav-link" href="Login">Logout</a></li>
				</ul>
			</div>
		</nav>
	</header>


<!-- body -->
  <div class="container">


	<div class="col-xs-4" align="right">
		<a href="UserCreate" class="text-info">新規登録</a>
	</div>

	<!-- 検索ボックス -->
	<div class="search-form-area">
		<div class="panel-body">
			<form method="post" action="UserList" class="form-horizontal">
				<div class="form-group row">
					<label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="loginId"
							id="loginId">
					</div>
				</div>

				<div class="form-group row">
					<label for="userName" class="col-sm-2 col-form-label">ユーザ名</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="userName"
							id="userName">
					</div>
				</div>

				<div class="form-group row">

					<label for="birthDate" class="col-sm-2 col-form-label">生年月日</label>

					<div class="row col-sm-10">
						<div class="col-sm-5">
							<input type="date" name="birthDate" id="birthDate"
								class="form-control" />
						</div>

						<div class="col-sm-1 text-center">~</div>
						<div class="col-sm-5">
							<input type="date" name="date-end" id="date-end"
								class="form-control" />
						</div>
					</div>
				</div>
				 <div class="submit-button-area">
		<button type="submit" class="btn btn-outline-info btn-lg btn-block" value="検索">検索</button>
		</div>
			</form>
		</div>
	</div>

	<!-- 検索結果一覧 -->
	<div class="table-responsive">
		<table class="table table-striped">
			<thead class="thead-light">
				<tr>
					<th>ログインID</th>
					<th>ユーザ名</th>
					<th>生年月日</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="user" items="${userList}">
					<tr>
						<td>${user.loginId}</td>
						<td>${user.name}</td>
						<td>${user.birthDate}</td>
						<td><c:if test="${userInfo.loginId=='admin'}">
								<a class="btn btn-outline-info"
									href="UserDetail?id=${user.id}">詳細</a>
								<a class="btn btn-outline-info"
									href="UserUpdate?id=${user.id}">更新</a>
								<a class="btn btn-outline-info" href="UserDelete?id=${user.id}">削除</a>
							</c:if> <c:if test="${!(userInfo.loginId=='admin')}">
								<a class="btn btn-outline-info"
									href="UserDetail?id=${user.id}">詳細</a>

								<c:if test="${userInfo.loginId==user.loginId}">

									<a class="btn btn-outline-info"
										href="UserUpdate?id=${user.id}">更新</a>
								</c:if>

							</c:if>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	</div>
</body>
</html>