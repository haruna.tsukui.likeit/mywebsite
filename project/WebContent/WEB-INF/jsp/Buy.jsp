<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>購入</title>

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/original/common.css">
</head>
<body>

	<!-- header -->
	<header>
		<nav class="navbar navbar-expand-lg navbar-light"
			style="background-color: #17a2b8;">
			<a class="navbar-brand" href="Index">EC</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarNav" aria-controls="navbarNav"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<li class="nav-item"><a class="nav-link" href="UserData">User</a></li>
					<li class="nav-item"><a class="nav-link" href="Cart">Cart</a></li>
					<li class="nav-item"><a class="nav-link" href="Login">Logout</a></li>
				</ul>
			</div>
		</nav>
	</header>
	<!-- /header -->


	<div class="container">
		<div class="row center">
			<h5 class=" col s12">購入</h5>
		</div>
		<div class="row">
			<div class="section"></div>
			<div class="col s12">


						<div class="row">
								<table class="table">
								<thead>
									<tr>
										<th class="center" style="width:40%">商品名</th>
										<th class="center">単価</th>
										<th class="center"  style="width:20%">小計</th>
									</tr>
								</thead>
								<tbody>
										<c:forEach var="cartInItem" items="${cart}">
											<tr>
												<td class="center">${cartInItem.name}</td>
												<td class="center">${cartInItem.price}円</td>
												<td class="center">${cartInItem.price}円</td>
											</tr>
										</c:forEach>
										<tr>
											<td class="center"></td>
											<td class="center"></td>
											<td class="center">
											<form action="BuyConfirm" method="post">
												<div class="input-field ">
													<select name="delivery_method_id">
														<c:forEach var="dmdb" items="${dmdbList}">
															<option value="${dmdb.id}">${dmdb.name}</option>
														</c:forEach>
													</select> <label>配送方法</label>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="row">
								<div class="btn-area">

										<button class="btn btn-outline-info "
											type="submit">購入確認</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>

</body>
</html>