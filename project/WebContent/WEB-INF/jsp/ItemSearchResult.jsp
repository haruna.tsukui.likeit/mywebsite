<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>検索結果</title>

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/original/common.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<body>
	<!-- header -->
	<header>
		<nav class="navbar navbar-expand-lg navbar-light"
			style="background-color: #17a2b8;">
			<a class="navbar-brand" href="Index">EC</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarNav" aria-controls="navbarNav"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<li class="nav-item"><a class="nav-link" href="UserData">User</a></li>
					<li class="nav-item"><a class="nav-link" href="Cart">Cart</a></li>
					<li class="nav-item"><a class="nav-link" href="Login">Logout</a></li>
				</ul>
			</div>
		</nav>
	</header>
	<!-- /header -->

	<div class="section no-pad-bot" id="index-banner">
		<div class="container">
			<div class="row center">
				<div class="input-field col s8 offset-s2 ">
					<form action="ItemSearchResult" method="get"
						class="form-inline search">
						<input class="form-control mr-sm-2" type="text"
							placeholder="Search" aria-label="Search" name="search_word"
							value="${searchWord}">
						<button class="btn btn-outline-info " type="submit">Search</button>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row center">
			<h5 >検索結果</h5>
			</div>
			<p>${itemCount}件</p>

		<div class="section">
			<!--   商品情報   -->
			<div class="row">
				<c:forEach var="item" items="${itemList}" varStatus="status">
					<div class="col-3">
						<div class="card">
							<div class="card-image-style">
								<a href="Item?item_id=${item.id}&page_num=${pageNum}"><img
									 class="imgSize" src="img/${item.fileName}"></a>
							</div>
							<div class="card-content">
								<span class="card-title">${item.name}</span>
								<p>${item.price}円</p>
							</div>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>

	</div>
</body>
</html>