<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>login</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/original/login.css">

</head>
<body class="text-center">

	<form class="form-signin" action="Login" method="post">


		<c:if test="${erMsg != null}">
			<div class="alert alert-danger" role="alert">${erMsg}</div>
		</c:if>

		<h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
		<label for="loginId" class="sr-only"  >loginId</label> <input
			type="text" name="loginId" value="${inputLoginId}" class="form-control" placeholder="loginId"
			required autofocus> <label for="password" class="sr-only">password</label>
		<input type="text" name="password" class="form-control"
			placeholder="password" required> <br>
		<button class="btn btn-outline-info" type="submit">Signin</button>
	</form>

</body>
</html>