<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>userDetail</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/original/common.css">

</head>
<body>


	<!-- header -->
	<header>
		<nav class="navbar navbar-expand-lg navbar-light"
			style="background-color: #17a2b8;">
			<a class="navbar-brand" href="Index">EC</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarNav" aria-controls="navbarNav"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<li class="nav-item"><a class="nav-link" href="UserData">User</a></li>
					<li class="nav-item"><a class="nav-link" href="Cart">Cart</a></li>
					<li class="nav-item"><a class="nav-link" href="Login">Logout</a></li>
				</ul>
			</div>
		</nav>
	</header>
	<!-- /header -->



<div class="userDetail">
	<form method="post" action="#" class="form-horizontal">
		<div class="form-group row">
			<label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
			<div class="col-sm-10">
				<p class="form-control-plaintext">${user.loginId}</p>
			</div>
		</div>

		<div class="form-group row">
			<label for="userName" class="col-sm-2 col-form-label">ユーザ名</label>
			<div class="col-sm-10">
				<p class="form-control-plaintext">${user.name}</p>
			</div>
		</div>

		<div class="form-group row">
			<label for="birthDate" class="col-sm-2 col-form-label">生年月日</label>
			<div class="col-sm-10">
				<p class="form-control-plaintext">${user.birthDate}</p>
			</div>
		</div>


		<div class="form-group row">
			<label for="address" class="col-sm-2 col-form-label">住所</label>
			<div class="col-sm-10">
				<p class="form-control-plaintext">${user.address}</p>
			</div>
		</div>


		<div class="form-group row">
			<label for="createDate" class="col-sm-2 col-form-label">新規登録日時</label>
			<div class="col-sm-10">
				<p class="form-control-plaintext">${user.createDate}</p>
			</div>
		</div>

		<div class="form-group row">
			<label for="updateDate" class="col-sm-2 col-form-label">更新日時</label>
			<div class="col-sm-10">
				<p class="form-control-plaintext">${user.updateDate}</p>
			</div>
		</div>





		<div class="col-xs-4">
			<a href="UserList" class="text-info">戻る</a>
		</div>

	</form>
</div>

</body>
</html>