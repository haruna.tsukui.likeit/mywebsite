<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>userCreate</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/original/common.css">



</head>
<body>


	<!-- header -->
	<header>
		<nav class="navbar navbar-expand-lg navbar-light"
			style="background-color: #17a2b8;">
			<a class="navbar-brand" href="Index">EC</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarNav" aria-controls="navbarNav"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<li class="nav-item"><a class="nav-link" href="UserData">User</a></li>
					<li class="nav-item"><a class="nav-link" href="Cart">Cart</a></li>
					<li class="nav-item"><a class="nav-link" href="Login">Logout</a></li>
				</ul>
			</div>
		</nav>
	</header>
	<!-- /header -->

	<!-- body -->
	<div class="form-area">

		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>

		<form action="UserCreate" method="post"  class="form-horizontal">
			<div class="form-group row">
				<label for="loginId" class="col-sm-4 col-form-label">ログインID</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" name="loginId" value="${inputLoginId}">
				</div>
			</div>

			<div class="form-group row">
				<label for="password" class="col-sm-4 col-form-label">パスワード</label>
				<div class="col-sm-8">
					<input type="password" class="form-control" name="password">
				</div>
			</div>

			<div class="form-group row">
				<label for="passwordConf" class="col-sm-4 col-form-label">パスワード(確認)</label>
				<div class="col-sm-8">
					<input type="password" class="form-control" name="passwordConf">
				</div>
			</div>

			<div class="form-group row">
				<label for="userName" class="col-sm-4 col-form-label">ユーザ名</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" name="userName" value="${inputuserName}">
				</div>
			</div>

			<div class="form-group row">
				<label for="birthDate" class="col-sm-4 col-form-label">生年月日</label>
				<div class="col-sm-8">
					<input type="date" class="form-control" name="birthDate" value="${inputbirthDate}">
				</div>
			</div>


			<div class="form-group row">
				<label for="address" class="col-sm-4 col-form-label">住所</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" name="address" value="${inputaddress}">
				</div>
			</div>


			<div class="submit-button-area">
				<button class="btn btn-outline-info btn-lg btn-block" type="submit">登録</button>
			</div>
		</form>
</body>
</html>