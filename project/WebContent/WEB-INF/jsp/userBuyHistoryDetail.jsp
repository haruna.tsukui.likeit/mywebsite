<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>購入履歴詳細</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/original/common.css">
</head>
<body>

	<!-- header -->
	<header>
		<nav class="navbar navbar-expand-lg navbar-light"
			style="background-color: #17a2b8;">
			<a class="navbar-brand" href="Index">EC</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarNav" aria-controls="navbarNav"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<li class="nav-item"><a class="nav-link" href="UserData">User</a></li>
					<li class="nav-item"><a class="nav-link" href="Cart">Cart</a></li>
					<li class="nav-item"><a class="nav-link" href="Login">Logout</a></li>
				</ul>
			</div>
		</nav>
	</header>
	<!-- /header -->

	<div class="container">
		<div class="row center">
			<h4 class=" col-12">購入詳細</h4>
		</div>
		<!--  購入 -->
		<div class="row">
			<div class="col-12">


				<table class="table ">
					<thead>
						<tr>
							<th class="center" style="width: 20%;">購入日時</th>
							<th class="center">配送方法</th>
							<th class="center" style="width: 20%">合計金額</th>
						</tr>
					</thead>
					<tbody>
						<td class="center">${bdb.formatDate}</td>
						<td class="center">${bdb.deliveryMethodName}</td>
						<td class="center">${bdb.totalPrice}円</td>
					</tbody>
				</table>

			</div>
		</div>
		<!-- 詳細 -->
		<div class="row">
			<div class="col-12">


				<table class="table">
					<thead>
						<tr>
							<th class="center">商品名</th>
							<th class="center" style="width: 20%">単価</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="buyIDB" items="${buyIDBList}">
							<tr>
								<td class="center">${buyIDB.name}</td>
								<td class="center">${buyIDB.price}円</td>
						</c:forEach>
						<tr>
							<td class="center">${resultBDB.deliveryMethodName}</td>
							<td class="center">${resultBDB.deliveryMethodPrice}円</td>
						</tr>


					</tbody>
				</table>
			</div>
		</div>
	</div>

</body>
</html>