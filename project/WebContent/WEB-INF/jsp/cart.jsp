<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>カート</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/original/common.css">
<body>

	<!-- header -->
	<header>
		<nav class="navbar navbar-expand-lg navbar-light"
			style="background-color: #17a2b8;">
			<a class="navbar-brand" href="Index">EC</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarNav" aria-controls="navbarNav"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<li class="nav-item"><a class="nav-link" href="UserData">User</a></li>
					<li class="nav-item"><a class="nav-link" href="Cart">Cart</a></li>
					<li class="nav-item"><a class="nav-link" href="Login">Logout</a></li>
				</ul>
			</div>
		</nav>
	</header>
	<!-- /header -->

	<div class="container">
		<div align="center">
			${cartActionMessage}

			<h5 align="center">買い物かご</h5>
		</div>

		<div class="section">
			<form action="ItemDelete" method="POST">
				<div class="row justify-content-between">
					<button class="btn btn-outline-info" type="submit" name="action">
						削除</button>

					<a href="Buy" class="btn btn-outline-info">レジに進む</a>

				</div>



				<br>
				<div class="row">



					<c:forEach var="item" items="${cart}" varStatus="status">
						<div class="col-3">
							<div class="card">
								<div class="card-image-style">
									<a href="Item?item_id=${item.id}"><img class="imgSize"
										src="img/${item.fileName}"> </a>
								</div>
								<div class="card-content">
									<span class="card-title">${item.name}</span>
									<p>${item.price}円</p>
									<p>
										<input type="checkbox" id="${status.index}"
											name="delete_item_id_list" value="${item.id}" /> <label
											for="${status.index}">削除</label>
									</p>
								</div>
							</div>
						</div>
					</c:forEach>






				</div>
			</form>
		</div>
	</div>


</body>
</html>